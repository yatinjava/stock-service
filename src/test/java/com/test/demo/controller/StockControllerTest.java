package com.test.demo.controller;

import com.test.demo.config.CORSFilter;
import com.test.demo.constants.BeepConstants;
import com.test.demo.entity.StockDetail;
import com.test.demo.response.DataResponse;
import com.test.demo.response.Response;
import com.test.demo.service.StockDetailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StockController.class)
public class StockControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockDetailService stockDetailService;

    @InjectMocks
    private StockController stockController;

    @BeforeEach
     void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(stockController).addFilter(new CORSFilter()).build();
    }

    @Test
     void testhome() throws Exception {
        mockMvc.perform(get("/ping").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andDo(print());
    }

    /* Call the proper API with proper Data */
    @Test
     void getStockDetails_Found() throws Exception {
        Map<String, Object> response = new HashMap<>();
        response.put("stockDetail", getStockDetail());
        DataResponse dataResponse = new DataResponse(HttpStatus.OK, HttpStatus.OK.value(),
                BeepConstants.STOCK_DETAIL_FOUND, response);
        when(stockDetailService.getStockDetail("ICICI")).thenReturn(dataResponse);
        mockMvc.perform(get("/instrument/ICICI").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andDo(print());

    }

    /* Call the proper API with passing stockName which does not available */
    @Test
     void getStockDetails_Not_Found() throws Exception {
        Response response = new Response(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(),
                BeepConstants.STOCK_DETAIL_NOT_FOUND);
        when(stockDetailService.getStockDetail("TEST DUMMY")).thenReturn(response);
        mockMvc.perform(get("/instrument/TEST DUMMY").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());

    }

    /* Call the wrong API url with passing proper stockName */
    @Test
     void getStockDetails_Not_Found_API() throws Exception {
        Response response = new Response(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(),
                BeepConstants.STOCK_DETAIL_NOT_FOUND);
        when(stockDetailService.getStockDetail("TEST DUMMY")).thenReturn(response);
        mockMvc.perform(get("/instrument123/TEST DUMMY").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }



    private StockDetail getStockDetail() {
        StockDetail stockDetail = new StockDetail();
        stockDetail.setStocks_id(12l);
        stockDetail.setStockName("ICICI");
        stockDetail.setStockDescription("Bank name");
        stockDetail.setStockPrice(new BigDecimal(150.0));
        stockDetail.setStockDate(new Date());
        return stockDetail;
    }
}
