package com.test.demo.service;

import com.test.demo.constants.BeepConstants;
import com.test.demo.entity.StockDetail;
import com.test.demo.repository.StockDetailRepository;
import com.test.demo.response.Response;
import com.test.demo.service.impl.StockDetailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class StockDetailServiceTest {

    @InjectMocks
    private StockDetailServiceImpl stockDetailServiceImpl;

    @Mock
    private StockDetailRepository stockDetailRepository;

    @Test
    void getStockDetail() {
        Optional<StockDetail> stockDetail = Optional.of(new StockDetail());
        when(stockDetailRepository.findByStockName(Mockito.any())).thenReturn(stockDetail);
        Response response = stockDetailServiceImpl.getStockDetail("ICICI");
        assertEquals(HttpStatus.OK, response.getHttpStatus());
        assertEquals(BeepConstants.STOCK_DETAIL_FOUND, response.getResponseMessage());
    }

   @Test
    void getStockDetail_DataNotFound() {
        Response response = stockDetailServiceImpl.getStockDetail("abc");
        assertEquals(HttpStatus.NOT_FOUND, response.getHttpStatus());
        assertEquals(BeepConstants.STOCK_DETAIL_NOT_FOUND, response.getResponseMessage());
    }



}
