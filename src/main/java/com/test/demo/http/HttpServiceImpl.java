package com.test.demo.http;

import com.test.demo.constants.UrlConstants;
import com.test.demo.enums.ResponseEnums;
import com.test.demo.exception.AuthServerException;
import com.test.demo.exception.ResourceNotFoundException;
import com.test.demo.utils.JsonUtils;
import com.test.demo.exception.AuthResponse;
import com.test.demo.response.ResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.test.demo.constants.BeepConstants;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HttpServiceImpl implements HttpService {

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Value("${user.microservice.call}")
	public String baseUrl;

	@Override
	public Object clientRestCall(String url, Object object, HttpMethod httpMethod, MediaType mediaType, Object mockData,
			String token) {
		return HttpUtil.restCall(url, restTemplate, token, object, httpMethod, mediaType);
	}

	public Object hystrixFallback(Object mockData) {
		return mockData;
	}

	@Override
	public Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod, MediaType mediaType,
			String token) {
		try {
			Object obj = HttpUtil.restCall(url, restTemplate, token, object, httpMethod, mediaType);
			if(obj == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(null,500);
			}
			return obj;
		} catch (RestClientResponseException exception) {
			AuthResponse authResponse = JsonUtils.toObject(exception.getResponseBodyAsByteArray(), AuthResponse.class);
			if(authResponse == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(authResponse, exception.getRawStatusCode());
			}
			if(exception.getRawStatusCode() == 401 || exception.getRawStatusCode() == 400) {
				throw ResponseEnums.UNAUTHORIZED_CLIENT.authResponse(authResponse, exception.getRawStatusCode());
			}
			throw ResponseEnums.getFromId(authResponse.getCode()).authResponse(authResponse, exception.getRawStatusCode());
		}
	}

	@Override
	public Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod,
			HttpHeaders httpHeaders) {
		try {
			Object obj = HttpUtil.restCall(url, restTemplate, object, httpMethod, httpHeaders);
			if(obj == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(null, 500);
			}
			return obj;
		} catch (RestClientResponseException exception) {
			AuthResponse authResponse = JsonUtils.toObject(exception.getResponseBodyAsByteArray(), AuthResponse.class);
			if(exception.getRawStatusCode() == 401) {
				authResponse = new AuthResponse(ResponseCode.UNAUTHORIZED, BeepConstants.UNAUTHORIZED_ACCESS, null);
				String resturl = "";
				if(url.equals(resturl)){
					authResponse = new AuthResponse(ResponseCode.UNAUTHORIZED, BeepConstants.UNAUTHORIZED_EMAIL_MSG, null);
				}
				throw ResponseEnums.getFromId(authResponse.getCode()).authResponse(authResponse, authResponse.getCode());
			}
			if(authResponse == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(authResponse, exception.getRawStatusCode());
			}

			if(exception.getRawStatusCode() == 401 || exception.getRawStatusCode() == 400) {
				throw ResponseEnums.UNAUTHORIZED_CLIENT.authResponse(authResponse, exception.getRawStatusCode());
			}

			throw ResponseEnums.getFromId(authResponse.getCode()).authResponse(authResponse, exception.getRawStatusCode());
		}
	}

	@Override
	public Object clientRestCall(String url, Object object, HttpMethod httpMethod,
			HttpHeaders httpHeaders) {
		try {
			Object obj = HttpUtil.restCall(url, restTemplate, object, httpMethod, httpHeaders);
			if(obj == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(null, 500);
			}
			return obj;
		} catch (RestClientResponseException exception) {
			AuthResponse authResponse = JsonUtils.toObject(exception.getResponseBodyAsByteArray(), AuthResponse.class);
			if(authResponse == null) {
				throw ResponseEnums.INTERNAL_SERVER.authResponse(authResponse, exception.getRawStatusCode());
			}else if(exception.getRawStatusCode() == 400) {
				authResponse.setMessage(BeepConstants.UNAUTHORIZED_ACCESS);
				authResponse.setCode(ResponseCode.UNAUTHORIZED);
				throw ResponseEnums.getFromId(authResponse.getCode()).authResponse(authResponse, ResponseCode.UNAUTHORIZED);
			}
			throw ResponseEnums.getFromId(authResponse.getCode()).authResponse(authResponse, exception.getRawStatusCode());
		}
	}
	/**
	 * @param url
	 * @param object
	 * @param httpMethod
	 * @param httpHeaders
	 * @return
	 */
	public Object restCallWithErrorHandling(String url, Object object, HttpMethod httpMethod, HttpHeaders httpHeaders) {
		try {
			HttpEntity<String> entityCredentials = new HttpEntity<String>(object.toString(), httpHeaders);
			return restTemplate.exchange(url, HttpMethod.POST, entityCredentials, String.class);
		} catch (RestClientResponseException exception) {
			if (exception.getRawStatusCode() == 401) {
				throw new AuthServerException(exception.getRawStatusCode(),BeepConstants.UNAUTHORIZED_ACCESS,
						HttpStatus.UNAUTHORIZED);
			}
			if (exception.getRawStatusCode() == 404) {
				throw new ResourceNotFoundException("Information not found");
			}
			throw new AuthServerException(exception.getRawStatusCode(),exception.getResponseBodyAsString(),
					HttpStatus.NOT_FOUND);
		}
	}
}