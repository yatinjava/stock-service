package com.test.demo.constants;

public final class ParamConstants {

	private ParamConstants() {}

	/* auth parameter */
	public static final String USERID = "userId";
	public static final String ID = "id";
	public static final String USER_ID = "userid";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String FNAME = "fname";
	public static final String LNAME = "lname";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String SUCCESS = "success";
	public static final String ISVALID = "is_valid";
	public static final String REVOKED = "revoked";
	public static final String DEVICE_TYPE = "deviceType";

}
