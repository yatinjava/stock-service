package com.test.demo.constants;

public final class RegexConstant {

	private RegexConstant(){}

	public static final String TEXT = "^[a-zA-Z0-9 ]*$";
	public static final String NUMERIC = "^[0-9]*$";
	public static final String PHONE = "(0/91)?[6-9][0-9]{9}";
	public static final String PHONE_LOGIN = "(0/91)?[0-9]{10}";
	public static final String EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String LOGIN_USER = "(((0/91)?[6-9][0-9]{9}) | (^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$))";
	public static final String ALPHABETIC = "^[a-zA-Z ]*$";
	public static final String GSTNUMBER = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
	public static final String PANNUMBER = "[A-Z]{5}\\d{4}[A-Z]{1}";
	public static final String PINCODE = "^[0-9]{6}$";
	public static final String IFSCCODE = "^[a-zA-Z0-9]{11}$";
	public static final String WEBSITE = "((http?|https|ftp|file)://)?((W|w){3}.)?[a-zA-Z0-9]+\\.[a-zA-Z]+";
	public static final String ALPHANUMERIC = "^[a-zA-Z0-9]+$";
	public static final String ADMIN_PASSWORD = "((?!.* )(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{8,12})";
	public static final String SELLER_PASSWORD = "((?!.* )(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{8})";
	public static final String BUYER_PASSWORD = "((?!.* )(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{8,15})";
	public static final String DATE_FORMATE_DDMMYYYY = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$";
	public static final String LATITUDE = "^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$";
	public static final String LONGITUDE = "[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$";
	public static final String ALPHANUMERICSP = "^[A-Za-z0-9_@.,=()! \\/#&+-]*$";
}