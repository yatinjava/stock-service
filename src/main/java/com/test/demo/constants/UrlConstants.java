package com.test.demo.constants;

public final class UrlConstants {

	private UrlConstants(){}

	public static final String GET_INSTRUMENT = "/instrument/{stockName}";
	public static final String HEALTH_CHECK = "/healthCheck";

}
