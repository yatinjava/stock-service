package com.test.demo.constants;

public class BeepConstants {

    private BeepConstants() {

    }

    public static final String COMMON_ERROR_MSG = "Something went wrong internally";
    public static final String UNAUTHORIZED_ACCESS = "Unauthorized Access !";
    public static final String HttpStatus = "httpStatus";
    public static final String SUCCESS_RESPONSE = "Success";
    public static final String UNAUTHORIZED_EMAIL_MSG = "Please enter valid email address.";
    public static final String RESPONSE_CODE = "responseCode";
    public static final String RESPONSE_MESSAGE = "responseMessage";
    public static final String STOCK_DETAIL_NOT_FOUND = "Stock detail not found";
    public static final String WELCOME_MESSAGE = "Welcome to stock microservice........!";
    public static final String SUCCESS = "Success";

    public static final String STOCK_DETAIL_FOUND = "Get the stock detail data sucessfully";





}
	