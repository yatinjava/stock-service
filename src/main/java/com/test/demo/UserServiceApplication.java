package com.test.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserServiceApplication {

    public static void main(String[] args) {
        /* Note : for logger's different types of method understanding
         * you can remove after understand how to write logger in application.*/
        SpringApplication.run(UserServiceApplication.class, args);
    }

}
