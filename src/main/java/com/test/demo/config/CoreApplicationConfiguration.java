package com.test.demo.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This for load classpath configuration file.
 */
@Configuration
@EnableAutoConfiguration
@EntityScan({ "com.test.demo.entity" }) // entityscan and component scan we can exclude also because main annoation will
											// do this by default
@ComponentScan({ "com.test.demo.config", "com.test.demo.service", "com.test.demo.repository",
		"com.test.demo.controller", "com.test.demo.model", "com.test.demo", "com.test.demo.utils", "com.test.demo.http" })
@PropertySources({ @PropertySource("classpath:messages.properties"), @PropertySource("classpath:log4j.properties") })
@EnableTransactionManagement
@EnableAsync
public class CoreApplicationConfiguration {

	@Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
}