package com.test.demo.exception;

import java.util.List;

import com.test.demo.response.InvalidField;
import org.springframework.http.HttpStatus;

public class AuthServerException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private int code;
	private Object data;
	private List<InvalidField> trace;
	private HttpStatus httpStatus;
	

	public AuthServerException(int code, String message) {
		super(message);
		this.code = code;
	}
	
	public AuthServerException(int code, String message, Object data) {
		super(message);
		this.code = code;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public List<InvalidField> getTrace() {
		return trace;
	}

	public void setTrace(List<InvalidField> trace) {
		this.trace = trace;
	}

	public AuthServerException(int code, String message, List<InvalidField> trace , HttpStatus httpStatus) {
		super(message);
		this.code = code;
		this.trace = trace;
		this.httpStatus = httpStatus;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}