package com.test.demo.exception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;

import com.test.demo.constants.BeepConstants;
import com.test.demo.response.DataResponse;
import com.test.demo.response.InvalidField;
import com.test.demo.response.Response;
import com.test.demo.response.ResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author GHANSHYAM KUMAR
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = messageSource.getMessage("error.missing.param", new Object[]{ex.getParameterName()},
                LocaleContextHolder.getLocale());
        return new ResponseEntity<Object>(new Response(ResponseCode.BADREQUEST, error), HttpStatus.OK);
    }


    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(javax.validation.ConstraintViolationException ex) {
        List<InvalidField> invalidFields = new ArrayList<>();
        DataResponse response = new DataResponse();
        response.setResponseCode(ResponseCode.BADREQUEST);
        for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
            InvalidField invalidField = new InvalidField();
            invalidField.setMessage(constraintViolation.getMessage());
            String str = constraintViolation.getPropertyPath().toString();
            invalidField.setField(str.contains(".") ? str.substring(str.indexOf(".") + 1, str.length()) : str);
            invalidFields.add(invalidField);
        }
        response.setTrace(invalidFields);
        String invalidFieldName = invalidFields
                .stream()
                .map(a -> String.valueOf(a.getField()))
                .collect(Collectors.joining(","));
        response.setResponseMessage(invalidFieldName + " is incorrect");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        DataResponse response = new DataResponse();
        response.setResponseCode(ResponseCode.BADREQUEST);
        response.setHttpStatus(HttpStatus.BAD_REQUEST);
        response.setData(new HashMap<>());
        List<InvalidField> invalidFields = new ArrayList<>();
        if (!ex.getBindingResult().getFieldErrors().isEmpty()) {
            for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
                InvalidField invalidField = new InvalidField();
                invalidField.setField(fieldError.getField());
                invalidField.setMessage(fieldError.getDefaultMessage());
                invalidFields.add(invalidField);
            }
            response.setTrace(invalidFields);
        }
        String invalidFieldName = invalidFields
                .stream()
                .map(a -> String.valueOf(a.getField()))
                .collect(Collectors.joining(","));
        response.setResponseMessage(invalidFieldName + " is incorrect");
        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @ExceptionHandler(AuthServerException.class)
    protected ResponseEntity<Object> handleAuthServerException(final HttpServletRequest request,
                                                               final AuthServerException ex) {

        Response response = null;
        if (ex.getCode() == 401) {
            return buildResponseEntity(new Response(ResponseCode.UNAUTHORIZED, ex.getMessage()), HttpStatus.UNAUTHORIZED);
        }
        if (ex.getData() != null || !StringUtils.isEmpty(ex.getTrace())) {
            response = new DataResponse(ex.getCode(), ex.getMessage(), ex.getTrace());
        } else {
            response = new Response(ex.getCode(), ex.getMessage());
        }

        return buildResponseEntity(response, HttpStatus.OK);
    }

    @ExceptionHandler(RoleNotFoundException.class)
    protected ResponseEntity<Object> handleAuthServerException(final HttpServletRequest request,
                                                               final RoleNotFoundException ex) {
        Response response = new Response(ResponseCode.NOT_FOUND, ex.getMessage());
        return buildResponseEntity(response, HttpStatus.OK);
    }

    @ExceptionHandler(InvalidDataException.class)
    protected ResponseEntity<Object> handleAuthServerException(final HttpServletRequest request,
                                                               final InvalidDataException ex) {
        Response response = new Response(ResponseCode.BADREQUEST, ex.getMessage());
        return buildResponseEntity(response, HttpStatus.OK);
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<Object> handleAuthServerException(final HttpServletRequest request,
                                                               final ResourceNotFoundException ex) {
        Response response = new Response(ResponseCode.NOT_FOUND, ex.getMessage());
        return buildResponseEntity(response, HttpStatus.OK);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex,
                                                                  WebRequest request) {
        if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            Response response = new Response(ResponseCode.INTERNALSERVER, BeepConstants.COMMON_ERROR_MSG);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        Response response = new Response(ResponseCode.INTERNALSERVER, BeepConstants.COMMON_ERROR_MSG);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private ResponseEntity<Object> buildResponseEntity(Response resposne, HttpStatus httpStatus) {
        return new ResponseEntity<>(resposne, httpStatus);
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.OK)
    protected ResponseEntity<Object> handleAuthServerException(final HttpServletRequest request,
                                                               final EmailAlreadyExistsException ex) {
        Response response = new Response(ResponseCode.BADREQUEST, ex.getErrorCode());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}