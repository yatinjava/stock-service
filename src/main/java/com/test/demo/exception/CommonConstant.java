package com.test.demo.exception;

public class CommonConstant {
    private CommonConstant(){

    }

    public static final String DATA = "Data";
    public static final String IHTTPCODE = "responseCode";
    public static final String BSTATUS = "bStatus";
    public static final String TMESSAGE = "responseMessage";
    
 	public static final String COMMON_EMAIL_VRFCTN_SUBJECT = "common.email.verification.subject";
 	public static final String COMMON_EMAIL_VRFCTN_EMAIL_BODY 	= "common.email.verification.emailbody";

}
