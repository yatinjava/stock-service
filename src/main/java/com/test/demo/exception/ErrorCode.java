package com.test.demo.exception;

public class ErrorCode {

	private ErrorCode() {
	}

	public static class Common {
		private Common() {
		}

		public static final String ID_MUST_NOT_NULL = "id.is.null";
		public static final String LIST_IS_EMPTY = "record.is.empty";
		public static final String CITY_IS_EMPTY = "city.is.empty";
		public static final String SELLER_NOT_EXISTS = "seller.not.exist";
		public static final String USERADDRESS_NOT_EXISTS ="user.address.not.exit";
		public static final String SOMETHING_WENT_WRONG = "something.went.wrong";
		public static final String BUYER_NOT_EXISTS = "buyer.not.exist";
		public static final String ADDRESS_NOT_EXISTS = "address.not.exist";

	}

	public static class UserAddressError {

		private void UserAddressError() {
		}

		public static final String RECORD_NOT_FOUND = "error.record.not.found";
		public static final String ADDRESS_NOT_EMPTY = "msg.address.error-1";
		public static final String RECORD_ALREADY_EXIST = "msg.address.error-2";
		public static final String LIST_NOT_FOUND = "msg.trend.product.error-1";

	}
}