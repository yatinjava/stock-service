package com.test.demo.repository;

import com.test.demo.entity.StockDetail;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface StockDetailRepository extends JpaRepository<StockDetail, Long> {

    Optional<StockDetail> findByStockName(String stockName);

}

