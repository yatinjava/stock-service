package com.test.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "stock_details")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockDetail {

    private static final long serialVersionUID = 166L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stocks_id;

    @Column(name = "stock_name")
    private String stockName;

    @Column(name = "stock_description")
    private String stockDescription;

    @Column(name = "stock_price")
    private BigDecimal stockPrice;

    @CreatedDate
    @CreationTimestamp
    @Column(name = "stock_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date stockDate;

}
