package com.test.demo.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidateUsername.class)
public @interface UsernameValidator {
    public String message() default "{error.login.text}";

    String test() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
