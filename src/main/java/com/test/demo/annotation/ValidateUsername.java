package com.test.demo.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.test.demo.constants.RegexConstant;

public class ValidateUsername implements ConstraintValidator<UsernameValidator, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext ctx) {
		boolean status = false;
		if (validEmail(value) || validPhone(value)) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}

	private boolean validEmail(String userName) {
		boolean status = false;
		if (userName.matches(RegexConstant.EMAIL)) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}

	private boolean validPhone(String userName) {
		boolean status = false;
		if (userName.matches(RegexConstant.PHONE_LOGIN)) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}
}
