package com.test.demo.controller;

import com.test.demo.constants.BeepConstants;
import com.test.demo.constants.UrlConstants;
import com.test.demo.response.Response;
import com.test.demo.service.StockDetailService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@ApiResponses({@ApiResponse(code = 200, message = "Successful."),
        @ApiResponse(code = 204, message = "Successful, no content."),
        @ApiResponse(code = 400, message = "Bad request. Validation failure."),
        @ApiResponse(code = 201, message = "Entity created"),
        @ApiResponse(code = 404, message = "Entity not found"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StockController extends BaseController {

    private static final Logger logger = LogManager.getLogger(StockController.class);

    @Autowired
    private StockDetailService stockDetailService;


    @ApiOperation("Check availability of ms-promotions")
    @GetMapping(value = "/ping")
    public String home() {
        return BeepConstants.WELCOME_MESSAGE;
    }

    @ApiOperation("Stock MS: Health-Check")
    @GetMapping(value = UrlConstants.HEALTH_CHECK, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> healthCheck() {
        logger.info("Got request to check health status");
        return ResponseEntity.ok(new Response(HttpStatus.OK, HttpStatus.OK.value(), BeepConstants.SUCCESS));
    }

    /**
     * Get Stock details
     *
     * @return Stock details
     */
    @ApiOperation("Get Stock details")
    @GetMapping(value = UrlConstants.GET_INSTRUMENT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> geStockDetail(@PathVariable("stockName") String stockName) {
        logger.info("Got request to get stock detail call");
        Response response = stockDetailService.getStockDetail(stockName);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
