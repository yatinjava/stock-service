package com.test.demo.service;

import com.test.demo.response.Response;


public interface StockDetailService {

    /**
     *
     * @return stockDetail
     */
    Response getStockDetail(String stockName);
}
