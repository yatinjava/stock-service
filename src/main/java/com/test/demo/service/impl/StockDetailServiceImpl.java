package com.test.demo.service.impl;

import com.test.demo.constants.BeepConstants;
import com.test.demo.entity.StockDetail;
import com.test.demo.repository.StockDetailRepository;
import com.test.demo.response.DataResponse;
import com.test.demo.response.Response;
import com.test.demo.service.StockDetailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StockDetailServiceImpl implements StockDetailService {
    @Autowired
    private StockDetailRepository stockDetailRepository;

    private static final Logger logger = LogManager.getLogger(StockDetailServiceImpl.class);


    @Override
    public Response getStockDetail(String stockName) {
        /* Assuming that Every-day some back-End process or cron job execute and it update the all share stock price every-day*/
        logger.info("Get stock detail for stockName: {}",stockName);

        Optional<StockDetail> stockDetail = stockDetailRepository.findByStockName(stockName);
        if (stockDetail.isPresent()) {
            Map<String, Object> response = new HashMap<>();
            response.put("stockDetail", stockDetail.get());
            logger.info("Get sucessfully stock detail for stockName: {}",stockName);

            return new DataResponse(HttpStatus.OK, HttpStatus.OK.value(), BeepConstants.STOCK_DETAIL_FOUND, response);
        }
        logger.info("No stock data found");
        return new Response(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), BeepConstants.STOCK_DETAIL_NOT_FOUND);
    }
}
