package com.test.demo.enums;

public enum ReviewTypeEnum {

    SELLER("Seller", 1),
    BUYER("Buyer", 2),
    PRODUCT("Product", 3),
    STORE("Store", 4);

    private final int id;
    private final String reviewType;

    ReviewTypeEnum(String reviewType, int id) {
        this.reviewType = reviewType;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getReviewType() {
        return reviewType;
    }
}
