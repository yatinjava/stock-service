package com.test.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {
	private Long id;
	private String bookTitle;
	private String bookAuthor;
	private String bookCategory;
	private String status;
	private String dueDate="";
}
