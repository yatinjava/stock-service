package com.test.demo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DateUtils {
	private static final Logger logger = LogManager.getLogger(DateUtils.class);
	private static final  ThreadLocal<SimpleDateFormat> DEFAULT_FORMAT_WITH_TIME = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};
	
	private static final  ThreadLocal<SimpleDateFormat> DEFAULT_FORMAT_WITHOUT_TIME = new ThreadLocal<SimpleDateFormat>() {
		@Override
    	protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};

	private DateUtils() {
	}
	
	public static Date toDate(String date, SimpleDateFormat dateFormat) throws ParseException {
		return dateFormat.parse(date);
	}

	public static Date getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}
	
	public static Date toDateWithoutTime(String date) throws ParseException {
        return DEFAULT_FORMAT_WITHOUT_TIME.get().parse(date);
    }

    public static Date toDateWithTime(String date) throws ParseException {
        return DEFAULT_FORMAT_WITH_TIME.get().parse(date);
    }


    public static String toStringWithoutTime(Date date) {
        return DEFAULT_FORMAT_WITHOUT_TIME.get().format(date);
    }

    public static String toStringWithTime(Date date)  {
        return DEFAULT_FORMAT_WITH_TIME.get().format(date);
    }
}
